const express = require('express');
const axios = require('axios');
var bodyParser = require('body-parser')
const app = express();

app.use(bodyParser.json());

let valute;

axios
    .get('https://www.cbr-xml-daily.ru/daily_json.js')
    .then(res => valute = res.data.Valute);

app.post('/', function (req, res) {
    const { products } = req.body;
    const EUR = valute.EUR.Value;
    const USD = valute.USD.Value;
    let rubPrice = 0;
    products.map(product => {
        switch (product.currency) {
            case 'RUB':
                rubPrice = rubPrice + (product.price * product.quantity);
                break;
            case 'EUR':
                rubPrice = rubPrice + (product.price * product.quantity * EUR);
                break;
            case 'USD':
                rubPrice = rubPrice + (product.price * product.quantity * USD);
                break;
        }
    });

    const eurPrice = rubPrice / EUR;
    const usdPrice = rubPrice / USD;
    
    const responce = [
        {
            currency: 'RUB',
            price: rubPrice
        },
        {
            currency: 'EUR',
            price: eurPrice
        },
        {
            currency: 'USD',
            price: usdPrice
        },
    ]

    res.send(responce);
});

app.listen(5000, function () {
    console.log('App listening on port 5000!');
});
