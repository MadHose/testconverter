import React from "react";
import { Table, Button } from 'antd';

const PriceConverter = ({convertRequest, convertValue}) => {
    const columns = [
        {
            title: 'Currency',
            dataIndex: 'currency',
            key: 'currency',
        },
        {
            title: 'Price',
            dataIndex: 'price',
            key: 'price',
        },
    ];

    return (
    <div className="converter">
        <Button type="primary" onClick={convertRequest}>Convert</Button>
        <Table pagination={{hideOnSinglePage: true}} dataSource={convertValue} columns={columns}/>
    </div>
    );
}

export default PriceConverter;