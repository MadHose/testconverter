import React, { useState } from "react";
import { Input, Select, Button } from 'antd';

const { Option } = Select;

const ProductAppender = ({addProduct, products}) => {
    
    const defaultProductValue = {name: '', quantity: 1, currency: "RUB", price: ''};
    const [product, changeProduct] = useState(defaultProductValue);

    const handleChange = (e) => {
        const {name, value} = e.target;
        const stateChanges = {...product};
        stateChanges[name] = value;
        changeProduct(stateChanges);
    }

    const handleCurrency = (value) => {
        changeProduct({...product, currency: value});
    }
    const isInteger = (value) => {
        const number = Number(value);
        return Number.isInteger(number) && isPositiveInteger(number) && !value.match(/\d+(\.)/);
    }

    const isPositiveInteger = (value) => {
        return value >= 0;
    }

    const handleAdd = () => {
        let valide = true;
        Object.values(product).forEach(el => {
            if (!el) {
                valide = false;
            }
        })
        if (!valide) {
            return null;
        }
        addProduct([...products, product]);
        changeProduct(defaultProductValue);
    }


    return (
        <div className="productAppender">
            <p>Add product to cart</p>
            <div className="valuesInput">
                <div>
                    <Input value={product.name} onChange={handleChange} name="name" placeholder="Name"/>
                    <Input value={product.quantity} defaultValue="1" onChange={(e) => isInteger(e.target.value) ? handleChange(e) : null} name="quantity" placeholder="Quantity"/>
                    <Select className="currency" defaultValue="RUB" onChange={handleCurrency} name="currency">
                        <Option value="RUB">RUB</Option>
                        <Option value="EUR">EUR</Option>
                        <Option value="USD">USD</Option>
                    </Select>
                    <Input value={product.price} onChange={(e) => isPositiveInteger(e.target.value) ? handleChange(e) : null} name="price" placeholder="Price"/>
                </div>
                <Button className="addButton" type='number' type="primary" onClick={handleAdd}>
                    Add
                </Button>
            </div>
        </div>
    );
}

export default ProductAppender;