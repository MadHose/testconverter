import axios from 'axios';
import React, { useState } from "react";
import { Table } from 'antd';
import ProductAppender from './ProductAppender';
import PriceConverter from './PriceConverter';

const List = () => {

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Quantity',
            dataIndex: 'quantity',
            key: 'quantity',
        },
        {
            title: 'Currency',
            dataIndex: 'currency',
            key: 'currency',
        },
        {
            title: 'Price',
            dataIndex: 'price',
            key: 'price',
        }
    ];

    const [products, setProduct] = useState([]);
    const [convertValue, setCurrency] = useState([]);

    const convertRequest = async () => {
        const response = await axios({
            method: 'post',
            url: '/',
            data: {
                products
            }
        });

        setCurrency(response.data);
    }

    return (
    <div className="productList">
        <Table pagination={{pageSize: 5}} dataSource={products} columns={columns}/>
        <div className="infoBlock">
            <ProductAppender products={products} addProduct={setProduct}/>
            <PriceConverter convertValue={convertValue} convertRequest={convertRequest}/>
        </div>
    </div>
    );
}

export default List;